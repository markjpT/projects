json.extract! project, :title, :vision, :description
json.url project_url(project, format: :json)
