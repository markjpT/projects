# README

[![pipeline status](https://gitlab.com/mindroot/projects/badges/master/pipeline.svg)](https://gitlab.com/mindroot/projects/commits/master)
[![coverage report](https://gitlab.com/mindroot/projects/badges/master/coverage.svg)](https://gitlab.com/mindroot/projects/commits/master)

## Events
Events is a Platform, where the user can easily create, plan and organize a real Event.

[Website](http://projects.worldofmind.org/)

[Executive Summary](https://docs.google.com/document/d/1gHkXE7QiGYZpO5jlJ5DWNETh79ckR3CoRUveiXcWLLk/edit?usp=sharing)

[Git repository](https://gitlab.com/mindroot/projects)

### Development

Clone the project

```git clone https://gitlab.com/mindroot/projects.git```

Prepare the database

```rails db:create && rails db:migrate && rails db:seed```

Start the server

```rails s```

Open your browser and visti

```localhost:3000```
